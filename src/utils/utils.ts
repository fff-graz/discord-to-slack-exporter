import { Message, TextChannel } from 'discord.js';
import sanitize from 'sanitize-filename';
import fs from 'fs';
import { SlackMessage } from '../@types/SlackMessage';
import * as exportConfig from '../config/export.json';

/**
 * beautifys a channelName by replacing all dashes with underlines, and stripping out all the emojis
 * @param {channelName} channel.name the discord.js channel.name to beautify
 * @returns {channelName} a new channel name
 */
export function beautifyChannelName(channelName: string) {
  const oldChannelName = channelName;
  channelName = sanitize(channelName);
  channelName = channelName.substring(channelName.indexOf('-') + 1);
  channelName = channelName.replace(/-/g, '_');
  if (exportConfig.channels.archived.includes(oldChannelName)) {
    channelName = 'nö_' + channelName + '_archived_01';
    return channelName;
  }
  channelName = 'nö_' + channelName;
  return channelName;
}

/**
 * creates a SlackMessage object from a given discord.js message
 * @param {Message} message the discord.js message to convert
 * @param {TextChannel} channel the discord.js text channel of the message
 * @returns {SlackMessage} a new slack message object
 */
export function createSlackMessage(
  message: Message,
  channel: TextChannel
): SlackMessage {
  return {
    // convert to unix timestamp
    timestamp: Math.floor(message.createdTimestamp / 1000),
    channel: beautifyChannelName(channel.name),
    // ensure uniqueness of usernames
    author: `${message.author.username}`,
    message: message.content
  };
}

export function sortArrayByTimestamp(
  array: {
    timestamp: number | string;
    [key: string]: any;
  }[]
): any[] {
  // messages need to be sorted by timestamp
  // see https://slack.com/intl/en-gb/help/articles/360035354694-Move-data-to-Slack-using-a-CSV-or-text-file
  return array.sort((a, b) => {
    return a.timestamp > b.timestamp ? 1 : a.timestamp < b.timestamp ? -1 : 0;
  });
}

export function stringifySlackMessages(
  messages: SlackMessage[]
): Record<string, string>[] {
  // convert timestamp to string
  return messages.map((msg) => ({
    ...msg,
    timestamp: msg.timestamp.toString()
  }));
}

export function saveStringToFile(string: string, filename: string): void {
  try {
    console.log(`Saving .csv at ${filename}...`);
    fs.writeFileSync(filename, string);
    console.log(`${filename} saved!`);
  } catch (error) {
    console.error(error);
  }
}
// Made with love by Massi and Failkid05
